<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="box">
		<div class="navigation">
        			<form action="<spring:url value="/page${ pageId }"/>">
        				<select name="authorId">
        				<c:choose>
        				<c:when test="${empty searchCriteria.authorId}">
        					<option value="-1" >
                        			<spring:message code="select.author" />
                        	</option>
                         </c:when>
                         	<c:otherwise>
        						<c:forEach var="author" items="${authors}" >
        							<c:if test="${searchCriteria.authorId == author.authorId}">
        								<option value="-1" ><c:out value="${ author.authorName }" /></option>
        							</c:if>
        						</c:forEach>
                        </c:otherwise>
                         </c:choose>
        					<c:forEach var="author" items="${authors}" varStatus="status">
        						<option value="${ author.authorId }" >
        							<c:out value="${ author.authorName }" />
        						</option>
        					</c:forEach>
        				</select>
        				<nav id="ulDropdown" class="ulDropdown">
                        	<li id="ulDrop" class="first-child-li-dropdown">
                        	<c:choose>
                        	<c:when test="${empty searchCriteria.tagsId}">
                        	<span id="ulDropdownText"><spring:message code="select.tags" /></span>
                        	</c:when>
                        	<c:otherwise>
                        	<c:forEach var="searchTagId" items="${searchCriteria.tagsId}" varStatus="loop">
                        	<c:forEach var="tag" items="${tags}" >
                        <c:if test="${searchTagId == tag.tagId}"><span id="${ tag.tagName }">
                        <c:out value="${tag.tagName}"/><c:if test="${!loop.last}">,</c:if></span></c:if>
                        </c:forEach>
                        	</c:forEach>
                        		</c:otherwise>
                        		</c:choose>
                       </li>
                       <c:forEach var="tag" items="${tags}" varStatus="status">
                        	<li class="dropdownTags"><input class="checkboxes-create-news" type="checkbox" value="${ tag.tagId }" name="tagId"/><span><c:out value="${ tag.tagName }" /></span></li>
                       </c:forEach>
                        </nav>
                        <p><input type="submit" name="submit" class="filter" value="<fmt:message key="filter.filter"/>" /></p>
        				<p><input class="filter" type="submit" value="<fmt:message key="filter.filter"/>" /></p>
        			</form>
        			<form class ="reset" action="<spring:url value="/reset_filter"/>">
        				<p><input class="filter second"  type="submit" value="<fmt:message key="filter.reset"/>" /></p>
        			</form>
        		</div>

		<div class="news">
			<c:forEach var="news" items="${newsList}" varStatus="status">
				<div class="shortNews">
					<div class="title">
						<b><a href="<spring:url value="/news/${news.newsId}"/>"><c:out value="${news.title}" /></a></b>
						<span>
							(by <c:out value="${ news.author.authorName }" />)
						</span>
						<div class="dateForAdmin">
                        	<spring:message code="date.pattern" var="pattern"/>
                        	<fmt:formatDate  value="${news.modificationDate}" type="date"
                        	pattern="${pattern}" var="date" />
                        	${date}
                        </div>
					</div>
					<div class="shortText">
						<c:out value="${ news.shortText }" />
					</div>
					<div class="infoAdoutNews">
						<div class="tags">
							<c:forEach var="e" items="${news.tags}" varStatus="status">
								<c:out value="${ e.tagName }" />
							</c:forEach>
						</div>
						<div class="comments">
							<span class="comments">
								Comments(<c:out value="${ fn:length(news.comments) }" />)
							</span>
						</div>
						<div class="editlink"  >
							<a href="<spring:url value="/news/${news.newsId}"/>">
								<fmt:message key="news.view"/></a>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>

		<div class="paging">
				<c:if test="${pageId-2>0}">
					<a href="<spring:url value="/page${ pageId-2 }"/>">${pageId-2}</a>
				</c:if>
				<c:if test="${pageId-1>0}">
				<a href="<spring:url value="/page${ pageId-1 }"/>">${pageId-1}</a>
				</c:if>
				<a href="<spring:url value="/page${ pageId}"/>">${pageId}</a>
				<c:if test="${numOfPages >= pageId+1}">
					<a href="<spring:url value="/page${ pageId+1 }"/>">${pageId+1}</a>
				</c:if>
				<c:if test="${numOfPages >= pageId+2}">
					<a href="<spring:url value="/page${ pageId+2 }"/>">${pageId+2}</a>
				</c:if>
		</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
				$(".multi-dropbox").dropdownchecklist();
			}
	);
</script>