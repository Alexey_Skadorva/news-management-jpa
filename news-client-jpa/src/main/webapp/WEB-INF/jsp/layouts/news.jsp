<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="box">
  <a href="<spring:url value="/page${pageId}"/>"><fmt:message key="navigation.back"/></a><br/>
  <div class="newsMessage">
    <div class="title">
     <b> <c:out value="${ news.title }" /></b>
      <span>(by <c:out value="${ news.author.authorName }" />)</span>
    </div>
    <div class="dateForAdmin">
               <spring:message code="date.pattern" var="pattern"/>
               <fmt:formatDate  value="${generalNews.news.modificationDate}" type="date"
               pattern="${pattern}" var="date" />
               ${date}
        </div>
    <div class="fullText">
      <c:out value="${ news.fullText }" />
    </div>
    <div class="commentsNews">
      <c:if test="${news.comments!=null}">
        <c:forEach var="elem" items="${news.comments}" varStatus="status">
         <div class="commentDate">
             <spring:message code="date.pattern" var="pattern"/>
             <fmt:formatDate  value="${elem.creationDate}" type="date"
             pattern="${pattern}" var="date" />
             ${date}
         </div>
            <div class="commentText">
              <c:out value="${elem.commentText}"></c:out>
              <input type="hidden" name="newsId" class="form" value="${news.newsId}">
              <input type="hidden" name="commentId" value="${elem.commentId}"/>
            </div>
        </c:forEach>
      </c:if>
    </div>
  <form  method="POST" name="add_comment" class="addComment" action="<spring:url value="/add_comment"/>"
     onsubmit="return validateComment()">
     <input type="hidden" name="newsId" class="form" value="${news.newsId}">
     <input type="text" id="commentText" name="commentText" required/><br>
     <input type="submit" id="buttonAddComment" value="<fmt:message key="news.comment.add"/>" />
   </form>
   <div id="validateCommentSize" class="hidden-validate" ><fmt:message key="comment.validateCommentSize"/></div> </div>

   <c:if test="${not empty previousNewsId}" >
     <a class="previous" href="<spring:url value="/news/${ previousNewsId }"/>">
       <fmt:message key="navigation.previous"/><br/>
     </a>
   </c:if>
   <c:if test="${not empty nextNewsId}" >
     <a class="next" href="<spring:url value="/news/${ nextNewsId }"/>">
       <fmt:message key="navigation.next"/><br/>
     </a>
   </c:if>
 </div>