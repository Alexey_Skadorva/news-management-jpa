package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.*;
import com.epam.newsmanagement.utils.SearchCriteriaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

@Controller
public class NewsController {
    @Autowired
    TagService tagService;
    @Autowired
    AuthorService authorService;
    @Autowired
    NewsService newsService;
    @Autowired
    MainService mainService;
    @Autowired
    CommentsService commentsService;

    private int NUM_NEWS_ON_PAGE = 4;

    private int numOfPages;

    @RequestMapping(value = PAGE)
    public ModelAndView listOfNews(@PathVariable(value = "pageId") Integer pageId,
                                   @RequestParam(value = "authorId", required = false) Long authorId,
                                   @RequestParam(value = "tagId", required = false) List<Long> tagsId,
                                   ModelMap model, HttpSession session) throws ServiceException {
        SearchCriteria searchCriteria = SearchCriteriaHelper.checkFilters(authorId, tagsId, pageId, session);
        if (pageId == null) {
            pageId = 1;
        }
        List<News> news =
                newsService.findByFilters(searchCriteria, pageId, NUM_NEWS_ON_PAGE);

        int numNews = newsService.countNumNews(searchCriteria);
        session.setAttribute("numNews", numNews);
        List<Long> newsIds = SearchCriteriaHelper.calculateNumberOfNews(news);
        session.setAttribute("newsIds", newsIds);
        numOfPages = SearchCriteriaHelper.calculateNumberOfPages(numNews, NUM_NEWS_ON_PAGE);
        model.addAttribute("numOfPages", numOfPages);
        getTagsAndAuthors(model);
        model.addAttribute("newsList", news);
        return new ModelAndView(MAIN);
    }

    @RequestMapping(value = NEWS)
    public ModelAndView getNews(@PathVariable Long newsId, HttpSession session) throws ServiceException {

        News news = newsService.getNews(newsId);
        SearchCriteriaHelper.changePage(session, newsId, NUM_NEWS_ON_PAGE);

        Long nextId = mainService.findNextNewsId(newsId, session, numOfPages, NUM_NEWS_ON_PAGE);
        Long previousId = mainService.findPreviousNewsId(newsId, session, NUM_NEWS_ON_PAGE);

        session.setAttribute("news", news);
        session.setAttribute("nextNewsId", nextId);
        session.setAttribute("previousNewsId", previousId);
        return new ModelAndView(NEWS_VIEW);
    }

    @RequestMapping(value = RESET_FILTER)
    public ModelAndView resetFilter(ModelMap model, HttpSession session) throws ServiceException {
        session.setAttribute("searchCriteria", null);
        session.setAttribute("authorName", null);
        return listOfNews(null, null, null, model, session);
    }

    public void getTagsAndAuthors(ModelMap model) throws ServiceException {
        List<Author> authors = authorService.getAllAuthors();
        List<Tag> tags = tagService.getAllTags();
        model.addAttribute("authors", authors);
        model.addAttribute("tags", tags);
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler() {
        return new ModelAndView(ERROR);
    }
}
