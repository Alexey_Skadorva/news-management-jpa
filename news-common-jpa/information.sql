/**
 * Insert informatian in table Author
 */
INSERT INTO Author
VALUES(1,'Evgen Zazas');

INSERT INTO Author 
VALUES(2,'Aleksandr Ect');

INSERT INTO Author 
VALUES(3,'Mihail Babc');

INSERT INTO Author
VALUES(4,'Maksim Rusak');

INSERT INTO Author
VALUES(5,'Evgen Kasdor');

INSERT INTO Author 
VALUES(6,'Pavel Beric');

/**
 * Insert informatian in table Tag
 */
INSERT INTO Tag
VALUES(1,'war');

INSERT INTO Tag
VALUES(2,'peace');

INSERT INTO Tag
VALUES(3,'future');

INSERT INTO Tag
VALUES(4,'nature');

INSERT INTO Tag
VALUES(5,'happy');

INSERT INTO Tag
VALUES(6,'www');

/**
 * Insert informatian in table Comments
 */
INSERT INTO Comments
VALUES(1,'cool',TO_TIMESTAMP('2015-3-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS'),1);

 INSERT INTO Comments
VALUES(2,'bad',TO_TIMESTAMP('2015-3-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS'),2);

INSERT INTO Comments
VALUES(3,'cool',TO_TIMESTAMP('2015-3-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS'),3);

INSERT INTO Comments
VALUES(4,'cool',TO_TIMESTAMP('2015-3-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS'),1);
    
INSERT INTO Comments
VALUES(5,'bad',TO_TIMESTAMP('2015-3-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS'),1);

/**
 * Insert informatian in table News_Tag
 */
INSERT INTO News_Tag
VALUES (1,1);

INSERT INTO News_Tag
VALUES (1,2);

INSERT INTO News_Tag
VALUES (2,3);

INSERT INTO NeWs_Tag
VALUES (2,4);

INSERT INTO News_Tag
VALUES (3,5);

INSERT INTO News_Tag
VALUES (3,6);
  
/**
 * Insert informatian in table News_Author
 */
INSERT INTO News_Author
VALUES (1,1);

INSERT INTO News_Author
VALUES (1,2);

INSERT INTO News_Author
VALUES (2,3);

INSERT INTO News_Author
VALUES (2,4);

INSERT INTO News_Author
VALUES (3,5);

INSERT INTO News_Author
VALUES (3,6);
  
/**
 * Insert informatian in table News
 */
INSERT INTO News
VALUES(1,'One million Brazilians march calling for President Rousseff to go ',
'One million Brazilians march calling for President Rousseff to go ',
'One million Brazilians',TO_TIMESTAMP('2015-2-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS')
,TO_DATE('02/16/2015', 'MM/DD/YYYY'));

INSERT INTO News
VALUES(2,'One million Brazilians march calling for President Rousseff to go ',
  'One million Brazilians march calling for President Rousseff to go ',
  'One million Brazilians',TO_TIMESTAMP('2015-2-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS')
,TO_DATE('02/16/2015', 'MM/DD/YYYY'));

INSERT INTO News VALUES
 (3,'One million Brazilians march calling for President Rousseff to go ',
  'One million Brazilians march calling for President Rousseff to go ',
  'One million Brazilians',TO_TIMESTAMP('2015-2-16 12:20:30', 'YYYY-MM-DD HH24:MI:SS')
,TO_DATE('02/16/2015', 'MM/DD/YYYY'));