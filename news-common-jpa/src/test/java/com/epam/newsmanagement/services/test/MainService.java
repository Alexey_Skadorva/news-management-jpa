package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MainService {
    @InjectMocks
    MainServiceImpl mainService;
    @Mock
    NewsServiceImpl newsService;
    @Mock
    CommentsServiceImpl commentsService;
    @Mock
    AuthorServiceImpl authorService;
    @Mock
    TagServiceImpl tagService;
    @Mock
    News news;

    @Test
    public void testDeleteNews() throws ServiceException {
        List<Long> id = new ArrayList<>();
        id.add(1L);
        doNothing().when(commentsService).deleteCommentByNewsId(id.get(0));
        doNothing().when(newsService).deleteDependencyWithAuthor(id.get(0));
        doNothing().when(newsService).deleteDependencyWithTags(id.get(0));
        doNothing().when(newsService).deleteNews(id.get(0));
        mainService.deleteNews(id);
        verify(newsService).deleteNews(id.get(0));
        verify(newsService).deleteDependencyWithAuthor(id.get(0));
        verify(newsService).deleteDependencyWithTags(id.get(0));
        verify(commentsService).deleteCommentByNewsId(id.get(0));
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(commentsService);
    }

    @Test
    public void testAddNews() throws ServiceException {
        Long id = 1L;
        Long authorId = 1L;
        List<Long> tag = new ArrayList<>();
        Date date = new Date(new java.util.Date().getTime());
        when(newsService.addNews(news)).thenReturn(id);
        doNothing().when(authorService).addNewsAuthor(authorId, id);
        doNothing().when(tagService).addNewsTag(id, tag);
        mainService.addNews(news, authorId, tag, date);
        verify(newsService).addNews(news);
        verify(authorService).addNewsAuthor(authorId, id);
        verify(tagService).addNewsTag(id, tag);
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(authorService);
        verifyNoMoreInteractions(tagService);
    }

    @Test
    public void testGetAllInfoOfNews() throws ServiceException {
        Long id = 1L;
        when(commentsService.getCommentsForNews(id)).thenReturn(null);
        when(tagService.getTagsForNews(id)).thenReturn(null);
       // when(authorService.getAuthorForNews(id)).thenReturn(null);
        when(newsService.get(id)).thenReturn(null);
        mainService.getAllInfoAboutNews(id);
        verify(newsService).get(id);
        verify(commentsService).getCommentsForNews(id);
        //verify(authorService).getAuthorForNews(id);
        verify(tagService).getTagsForNews(id);
        verifyNoMoreInteractions(newsService);
        verifyNoMoreInteractions(authorService);
        verifyNoMoreInteractions(tagService);
        verifyNoMoreInteractions(commentsService);
    }

}
