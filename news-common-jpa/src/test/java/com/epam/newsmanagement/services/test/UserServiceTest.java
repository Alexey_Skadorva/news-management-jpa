package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    private final static String USER_NAME = "login";
    @InjectMocks
    UserServiceImpl userService;
    @Mock
    UserDao userDao;
    @Mock
    User user;

    @Test
    public void testFindUser() throws ServiceException, DAOException {
        User user = new User(1L, USER_NAME, USER_NAME, null, null);
        when(userDao.findByLogin(user.getLogin())).thenReturn(user);
        userService.findUser(user.getLogin());
        verify(userDao).findByLogin(user.getLogin());
        verifyNoMoreInteractions(userDao);
    }

    @Test
    public void testFindUserRole() throws ServiceException, DAOException {
        Long userId = 1L;
        when(userDao.findUserRole(userId)).thenReturn(null);
        userService.findUserRole(userId);
        verify(userDao).findUserRole(userId);
        verifyNoMoreInteractions(userDao);
    }
}