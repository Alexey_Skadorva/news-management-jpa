package com.epam.newsmanagement.services.test;

import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.impl.CommentsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentsServiceTest {
    @InjectMocks
    CommentsServiceImpl commentsService;
    @Mock
    CommentsDao commentsDao;
    @Mock
    Comment comment;

    @Test
    public void testDeleteComment() throws ServiceException, DAOException {
        Long commentId = 1L;
        doNothing().when(commentsDao).delete(commentId);
        commentsService.deleteComment(commentId);
        verify(commentsDao).delete(commentId);
        verifyNoMoreInteractions(commentsDao);
    }

//    @Test
//    public void testCreateComment() throws ServiceException, DAOException {
//        Long newsId = 1L;
//        Comment comment = new Comment(null,"www",null,null);
//        when(commentsDao.create(comment)).thenReturn(1L);
//        commentsService.addComment(comment.getCommentText(), newsId);
//        verify(commentsDao).create(comment);
//        verifyNoMoreInteractions(commentsDao);
//    }
}
