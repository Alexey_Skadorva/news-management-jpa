package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class NewsDaoImpl implements NewsDao {

    @Override
    public void edit(News news) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            session.update(news);
            Thread.sleep(3000); // To present optimistic lock
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Long create(News news) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            session.save(news);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return news.getNewsId();
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            News news = (News) session.load(News.class, newsId);
            news.setAuthor(null);
            news.setTags(null);
            session.delete(news);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public News get(Long newsId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        News news = null;
        try {
            news = (News) session.get(News.class, newsId);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return news;
    }

    @Override
    public List<News> findNewsByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNewsOnPage)
            throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        List<News> result = null;
        try {
            transaction = session.beginTransaction();
            String hql = buildHQLFilter(searchCriteria);
            Query query = session.createQuery(hql);
            insertSearchCriteria(query, searchCriteria);

            int startIndex;
            startIndex = (pageIndex - 1) * numNewsOnPage;
            query.setFirstResult(startIndex);
            query.setMaxResults(numNewsOnPage);
            result = query.list();
            if (result != null) {
                for (News temp : result) {
                    temp.getComments().size();//fetch lazy
                    temp.getTags().size();
                }
            }
            if (result.size() == 0) result = null;
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return result;
    }

    @Override
    public int countNumNews(SearchCriteria searchCriteria) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        int result = 0;
        try {
            transaction = session.beginTransaction();
            String hql = buildHQLFilter(searchCriteria);
            org.hibernate.Query query = session.createQuery(hql);
            insertSearchCriteria(query, searchCriteria);
            result = query.list().size();
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return result;
    }

    private String buildHQLFilter(SearchCriteria searchCriteria) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT DISTINCT n FROM News n ");

        if (searchCriteria.getTagsId() != null) {
            jpql.append("JOIN n.tags as t ");
        }
        jpql.append("WHERE ");
        if (searchCriteria.getTagsId() != null) {
            jpql.append(" (t.tagId IN (:tagIds) ) ");
        } else {
            jpql.append(" 1=1 ");
        }
        jpql.append(" AND");
        if (searchCriteria.getAuthorId() != null) {
            jpql.append(" (n.author.authorId = :authorId) ");
        } else {
            jpql.append(" 1=1 ");
        }

        jpql.append("ORDER BY size(n.comments) DESC , n.modificationDate DESC");
        return jpql.toString();
    }

    /**
     * Insert parameters from FilterVO into Query
     */
    private void insertSearchCriteria(org.hibernate.Query query, SearchCriteria searchCriteria) {

        if (searchCriteria.getTagsId() != null) {
            query.setParameterList("tagIds", searchCriteria.getTagsId());
        }
        if (searchCriteria.getAuthorId() != null) {
            query.setParameter("authorId", searchCriteria.getAuthorId());
        }

    }

    private void closeSession(Session session) {
        if (session.isOpen()) {
            session.close();
        }
    }
}
