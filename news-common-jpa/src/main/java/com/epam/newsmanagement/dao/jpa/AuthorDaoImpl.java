package com.epam.newsmanagement.dao.jpa;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.DaoUtil.closeManager;

@Transactional
public class AuthorDaoImpl implements AuthorDao {
    private EntityManagerFactory managerFactory;

    public AuthorDaoImpl() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }

    @Override
    public Author get(Long authorId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        Author author = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            author = manager.find(Author.class, authorId);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return author;
    }

    @Override
    public void edit(Author author) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Author tempAuthor = manager.find(Author.class, author.getAuthorId());
            tempAuthor.setAuthorName(author.getAuthorName());
            tempAuthor.setExpired(author.getExpired());
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

    @Override
    public Long create(String authorName) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        Author author = new Author(null, authorName, null);
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(author);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return author.getAuthorId();
    }

    @Override
    public List<Author> getAllAuthors() throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<Author> authors = new ArrayList<>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT a FROM Author a WHERE a.expired is null ORDER BY a.authorName");
            authors = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return authors;
    }
}
