package com.epam.newsmanagement.dao.hibernate;


import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import com.epam.newsmanagement.utils.JPAEntityManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.*;

import static com.epam.newsmanagement.utils.DaoUtil.closeManager;

public class UserDaoImpl implements UserDao {
    private EntityManagerFactory managerFactory;

    public UserDaoImpl() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }

    @Override
    public User findByLogin(String login) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {
            Criteria criteria = session.createCriteria(User.class);
            user = (User) criteria.add(Restrictions.eq("login", login)).uniqueResult();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return user;
    }

    @Override
    public String findUserRole(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        String role = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT r.roleName FROM Role r WHERE r.userId = :id");
            query.setParameter("id", id);
            role = (String) query.getSingleResult();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return role;
    }

    private void closeSession(Session session) {
        if (session.isOpen()) {
            session.close();
        }
    }

}