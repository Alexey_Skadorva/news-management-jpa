package com.epam.newsmanagement.dao.jpa;

import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import static com.epam.newsmanagement.utils.DaoUtil.closeManager;

@Transactional
public class CommentsDaoImpl implements CommentsDao {
    private EntityManagerFactory managerFactory;

    public CommentsDaoImpl() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }

    @Override
    public Long create(Comment comment) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(comment);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return comment.getCommentId();
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Comment comment = manager.find(Comment.class, commentId);
            System.out.print(comment);
            comment.setNews(null);
            manager.remove(comment);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

    @Override
    public Comment get(Long commentId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        Comment comment = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            comment = manager.find(Comment.class, commentId);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return comment;
    }
}
