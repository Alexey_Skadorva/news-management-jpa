package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.CloseResources;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.ColumnNames.TAG_ID;
import static com.epam.newsmanagement.utils.ColumnNames.TAG_NAME;

@Transactional
public class TagDaoImpl implements TagDao {
    private final static String ADD_TAG = "INSERT INTO TAG(TAG_ID,TAG_NAME) VALUES(TAG_SEQ.NEXTVAL,?)";
    private final static String EDIT_TAG = "UPDATE TAG SET TAG_NAME=? WHERE TAG_ID=?";
    private final static String ADD_DEPENDENCY_WITH_NEWS = "INSERT INTO  NEWS_TAG(NEWS_ID,TAG_ID) VALUES(?,?)";
    private final static String GET_NAME_BY_ID = "SELECT TAG_NAME FROM TAG WHERE TAG_ID=? ";
    private final static String GET_NAME_BY_NEWS_ID = "SELECT TAG.TAG_ID , TAG.TAG_NAME FROM TAG INNER JOIN NEWS_TAG ON "
            + "TAG.TAG_ID = NEWS_TAG.TAG_ID WHERE NEWS_TAG.NEWS_ID=?";
    private final static String GET_ID_BY_NAME = "SELECT TAG_ID FROM TAG WHERE TAG_NAME=?";
    private final static String GET_ALL_TAGS = "SELECT TAG_ID,TAG_NAME FROM TAG";
    private final static String DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID=?";
    private final static String DELETE_DEPENDENCY_WITH_NEWS = "DELETE FROM NEWS_TAG WHERE TAG_ID=?";
    private DataSource dataSource;
    private CloseResources closeResources;

    @Override
    public Tag getTagById(Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Tag tag = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_NAME_BY_ID);
            pst.setLong(1, tagId);
            rs = pst.executeQuery();
            if (rs.next()) {
                tag = new Tag(tagId, rs.getString(TAG_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return tag;
    }

    @Override
    public List<Tag> getAllTags() throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_ALL_TAGS);
            rs = pst.executeQuery();
            while (rs.next()) {
                Long tagId = rs.getLong(TAG_ID);
                String tagName = rs.getString(TAG_NAME);
                tags.add(new Tag(tagId, tagName));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return tags;
    }

    @Override
    public List<Tag> getByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String tagName = null;
        Long tagId = null;
        List<Tag> tags = new ArrayList<Tag>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_NAME_BY_NEWS_ID);
            pst.setLong(1, newsId);
            rs = pst.executeQuery();
            while (rs.next()) {
                tagName = rs.getString(TAG_NAME);
                tags.add(new Tag(tagId, tagName));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return tags;
    }

    @Override
    public boolean checkUniquenessTag(String tagName) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_ID_BY_NAME);
            pst.setString(1, tagName);
            rs = pst.executeQuery();
            if (rs.next()) {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return true;
    }

    @Override
    public void addTagForNews(Long newsId, List<Long> tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(ADD_DEPENDENCY_WITH_NEWS);
            for (Long i : tagId) {
                pst.setLong(1, newsId);
                pst.setLong(2, i);
                pst.addBatch();
            }
            pst.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    @Override
    public Long create(String name) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Long insertId = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {TAG_ID};
            pst = connection.prepareStatement(ADD_TAG, id);
            pst.setString(1, name);
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            if (rs.next()) {
                insertId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);

        }
        return insertId;
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_TAG);
            pst.setLong(1, tagId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public void deleteDependencyWithNews(Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_DEPENDENCY_WITH_NEWS);
            pst.setLong(1, tagId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }


    @Override
    public void edit(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(EDIT_TAG);
            pst.setLong(2, tag.getTagId());
            pst.setString(1, tag.getTagName());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    public void setCloseResources(CloseResources closeResources) {
        this.closeResources = closeResources;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
