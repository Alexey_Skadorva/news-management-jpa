package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.CloseResources;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.ColumnNames.AUTHOR_ID;
import static com.epam.newsmanagement.utils.ColumnNames.AUTHOR_NAME;

@Transactional
public class AuthorDaoImpl implements AuthorDao {
    private final static String ADD_AUTHOR = "INSERT INTO AUTHOR(AUTHOR_ID,AUTHOR_NAME) VALUES(AUTHOR_SEQ.NEXTVAL,?)";
    private final static String ADD_DEPENDENCY_WITH_NEWS = "INSERT INTO NEWS_AUTHOR(NEWS_ID,AUTHOR_ID) VALUES(?,?)";
    private final static String GET_NAME_BY_ID = "SELECT a.AUTHOR_NAME FROM AUTHOR a WHERE a.AUTHOR_ID=?";
    private final static String GET_NAME_BY_NEWS_ID = "SELECT AUTHOR.AUTHOR_ID , AUTHOR.AUTHOR_NAME FROM AUTHOR INNER JOIN NEWS_AUTHOR ON "
            + "AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_AUTHOR.NEWS_ID=?";
    private final static String GET_ID_BY_NAME = "SELECT AUTHOR_ID FROM AUTHOR WHERE AUTHOR_NAME=? ";
    private final static String GET_ALL_AUTHORS = "SELECT AUTHOR_ID,AUTHOR_NAME FROM AUTHOR WHERE AUTHOR.EXPIRED IS NULL";
    private final static String DELETE_AUTHOR = "DELETE FROM AUTHOR WHERE AUTHOR_ID=?";
    private final static String DELETE_DEPENDENCY_WITH_NEWS = "DELETE FROM NEWS_AUTHOR WHERE AUTHOR_ID=?";
    private final static String EDIT_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME=? WHERE AUTHOR_ID=?";
    private final static String EXPIRE_AUTHOR = "UPDATE AUTHOR SET EXPIRED=? WHERE AUTHOR_ID=?";
    private DataSource dataSource;
    private CloseResources closeResources;

    @Override
    public Author getAuthorById(Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Author author = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_NAME_BY_ID);
            pst.setLong(1, authorId);
            rs = pst.executeQuery();
            if (rs.next()) {
                author = new Author(authorId, rs.getString(AUTHOR_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return author;
    }

    @Override
    public void edit(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(EDIT_AUTHOR);
            pst.setLong(2, author.getAuthorId());
            pst.setString(1, author.getAuthorName());
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public List<Author> getAllAuthors() throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        List<Author> authors = new ArrayList<Author>();
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_ALL_AUTHORS);
            rs = pst.executeQuery();
            while (rs.next()) {
                Long authorId = rs.getLong(AUTHOR_ID);
                String authorName = rs.getString(AUTHOR_NAME);
                authors.add(new Author(authorId, authorName));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return authors;
    }

    @Override
    public boolean checkUniquenessAuthor(String authorName) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Long authorId = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_ID_BY_NAME);
            pst.setString(1, authorName);
            rs = pst.executeQuery();
            if (rs.next()) {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return true;
    }

    @Override
    public Author getByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Long authorId = null;
        String authorName = null;
        Author author = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(GET_NAME_BY_NEWS_ID);
            pst.setLong(1, newsId);
            rs = pst.executeQuery();
            if (rs.next()) {
                authorId = rs.getLong(AUTHOR_ID);
                authorName = rs.getString(AUTHOR_NAME);
            }
            author = new Author(authorId, authorName);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return author;
    }

    @Override
    public void addNewsAuthor(Long newsId, Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(ADD_DEPENDENCY_WITH_NEWS);
            pst.setLong(1, newsId);
            pst.setLong(2, authorId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }
    }

    @Override
    public Long create(String authorName) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        Long insertId = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            String[] id = {AUTHOR_ID};
            pst = connection.prepareStatement(ADD_AUTHOR, id);
            pst.setString(1, authorName);
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            if (rs.next()) {
                insertId = rs.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, rs, connection, dataSource);
        }
        return insertId;
    }

    @Override
    public void delete(Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_AUTHOR);
            pst.setLong(1, authorId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    @Override
    public void expire(Long authorId, Timestamp expired) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(EXPIRE_AUTHOR);
            pst.setTimestamp(1, expired);
            pst.setLong(2, authorId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    @Override
    public void deleteDependencyWithNews(Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement pst = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            pst = connection.prepareStatement(DELETE_DEPENDENCY_WITH_NEWS);
            pst.setLong(1, authorId);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeResources.close(pst, connection, dataSource);
        }

    }

    public void setCloseResources(CloseResources closeResources) {
        this.closeResources = closeResources;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
