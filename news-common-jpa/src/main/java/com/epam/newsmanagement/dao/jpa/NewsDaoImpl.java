package com.epam.newsmanagement.dao.jpa;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.DaoUtil.closeManager;

@Transactional
public class NewsDaoImpl implements NewsDao {
    private EntityManagerFactory managerFactory;

    public NewsDaoImpl() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }

    @Override
    public void edit(News news) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            News tempNews = manager.find(News.class, news.getNewsId());
            tempNews.setTitle(news.getTitle());
            tempNews.setShortText(news.getShortText());
            tempNews.setFullText(news.getFullText());
            tempNews.setCreationDate(news.getCreationDate());
            tempNews.setModificationDate(news.getModificationDate());
            tempNews.setAuthor(news.getAuthor());
            tempNews.setComments(news.getComments());
            tempNews.setTags(news.getTags());
            manager.merge(tempNews);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

    @Override
    public Long create(News news) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(news);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return news.getNewsId();
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;

        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            News news = manager.find(News.class, newsId);
            news.setAuthor(null);
            news.setTags(null);
            manager.remove(news);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

    @Override
    public News get(Long newsId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction = null;
        News news = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            news = manager.find(News.class, newsId);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return news;
    }

    @Override
    public List<News> findNewsByFilters(SearchCriteria searchCriteria, Integer pageIndex, int numNews)
            throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<News> result = new ArrayList<News>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            String jpql = buildJPQLFilter(searchCriteria);

            Query query = manager.createQuery(jpql);
            insertSearchCriteria(query, searchCriteria);

            int startIndex;
            startIndex = (pageIndex - 1) * numNews;
            query.setFirstResult(startIndex);
            query.setMaxResults(numNews);
            result = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }

    @Override
    public int countNumNews(SearchCriteria searchCriteria) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        int result = 0;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            String jpql = buildJPQLFilter(searchCriteria);
            Query query = manager.createQuery(jpql);
            insertSearchCriteria(query, searchCriteria);
            result = query.getResultList().size();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return result;
    }

    private String buildJPQLFilter(SearchCriteria searchCriteria) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT DISTINCT n FROM News n ");

        if (searchCriteria.getTagsId() != null) {
            jpql.append("JOIN n.tags as t ");
        }
        jpql.append("WHERE ");
        if (searchCriteria.getTagsId() != null) {
            jpql.append(" (t.tagId IN ?tagIds ) ");
        } else {
            jpql.append(" 1=1 ");
        }
        jpql.append(" AND");
        if (searchCriteria.getAuthorId() != null) {
            jpql.append(" (n.author.authorId = :authorId) ");
        } else {
            jpql.append(" 1=1 ");
        }

        jpql.append("ORDER BY size(n.comments) DESC , n.modificationDate DESC");
        return jpql.toString();
    }

    private void insertSearchCriteria(Query query, SearchCriteria searchCriteria) {

        if (searchCriteria.getTagsId() != null) {
            List<Long> tagIds = new ArrayList<Long>();
            for (Long id : searchCriteria.getTagsId()) {
                tagIds.add(id);
            }
            query.setParameter("tagIds", tagIds);
        }
        if (searchCriteria.getAuthorId() != null) {
            query.setParameter("authorId", searchCriteria.getAuthorId());
        }

    }
}
