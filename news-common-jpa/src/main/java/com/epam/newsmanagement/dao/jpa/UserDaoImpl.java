package com.epam.newsmanagement.dao.jpa;


import com.epam.newsmanagement.dao.UserDao;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;

import javax.persistence.*;

import static com.epam.newsmanagement.utils.DaoUtil.closeManager;

public class UserDaoImpl implements UserDao {
    private EntityManagerFactory managerFactory;

    public UserDaoImpl() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }

    @Override
    public User findByLogin(String login) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        User user = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT u FROM User u WHERE u.login = :login");
            query.setParameter("login", login);
            user = (User) query.getSingleResult();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return user;
    }

    @Override
    public String findUserRole(Long id) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        String role = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT r.roleName FROM Role r WHERE r.userId = :id");
            query.setParameter("id", id);
            role = (String) query.getSingleResult();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return role;
    }
}