package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class AuthorDaoImpl implements AuthorDao {

    @Override
    public Author get(Long authorId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Author author = null;
        try {
            author = (Author) session.get(Author.class, authorId);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return author;
    }

    @Override
    public void edit(Author author) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(author);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Long create(String authorName) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Author author = new Author(null, authorName, null);
        try {
            transaction = session.beginTransaction();
            session.save(author);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return author.getAuthorId();
    }

    @Override
    public List<Author> getAllAuthors() throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Author> result;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Author.class);
            criteria.add(Restrictions.isNull("expired"));
            criteria.addOrder(Order.asc("authorName"));
            result = criteria.list();
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return result;
    }

    private void closeSession(Session session) {
        if (session.isOpen()) {
            session.close();
        }
    }
}
