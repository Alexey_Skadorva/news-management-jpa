package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class CommentsDaoImpl implements CommentsDao {

    @Override
    public Long create(Comment comment) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(comment);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return comment.getCommentId();
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            Comment comment = (Comment) session.load(Comment.class, commentId);
            comment.setNews(null);
            session.delete(comment);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public Comment get(Long commentId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Comment comment = null;
        try {
            comment = (Comment) session.get(Comment.class, commentId);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return comment;
    }

    private void closeSession(Session session) {
        if (session.isOpen()) {
            session.close();
        }
    }

}
