package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

public interface TagDao {
    /**
     * Takes the input id of the tag
     * and with the help of the query finds out his name
     *
     * @param tagId
     * @return tag name
     * @throws DAOException
     */
    Tag get(Long tagId) throws DAOException;

    /**
     * Adds a new tag. Insert it into a table Tag. The tag's name
     * is passed to the method, and id is automatically generated and
     * returned after insertion
     *
     * @param tagName
     * @return insert id
     * @throws DAOException
     */
    Long create(String tagName) throws DAOException;

    /**
     * Removes the author of the Author table with a unique id.
     *
     * @param tagId
     * @throws DAOException
     */
    void delete(Long tagId) throws DAOException;

    /**
     * Returns all tags
     *
     * @return
     * @throws DAOException
     */
    List<Tag> getAllTags() throws DAOException;

    /**
     * Changes the tag who came on the parameters
     *
     * @param tag
     * @throws DAOException
     */
    void edit(Tag tag) throws DAOException;
}
