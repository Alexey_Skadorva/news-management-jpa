package com.epam.newsmanagement.dao.jpa;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.DaoUtil.closeManager;

@Transactional
public class TagDaoImpl implements TagDao {
    private EntityManagerFactory managerFactory;

    public TagDaoImpl() {
        managerFactory = JPAEntityManager.getEntityManagerFactory();
    }

    @Override
    public Tag get(Long tagId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        Tag tag = null;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            tag = manager.find(Tag.class, tagId);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return tag;
    }

    @Override
    public Long create(String name) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        Tag tag = new Tag(null, name, null);
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(tag);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return tag.getTagId();
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Tag tag = manager.find(Tag.class, tagId);
            //tag.setNews(null);
            manager.remove(tag);
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

    @Override
    public void edit(Tag tag) throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Tag tempTag = manager.find(Tag.class, tag.getTagId());
            tempTag.setTagName(tag.getTagName());
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
    }

    @Override
    public List<Tag> getAllTags() throws DAOException {
        EntityManager manager = null;
        EntityTransaction transaction;
        List<Tag> tags = new ArrayList<>();
        try {
            manager = managerFactory.createEntityManager();
            transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createQuery("SELECT a FROM Tag a ORDER BY a.tagName");
            tags = query.getResultList();
            transaction.commit();
        } catch (PersistenceException e) {
            throw new DAOException(e);
        } finally {
            closeManager(manager);
        }
        return tags;
    }
}
