package com.epam.newsmanagement.dao.hibernate;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class TagDaoImpl implements TagDao {

    @Override
    public Tag get(Long tagId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Tag tag = null;
        try {
            tag = (Tag) session.get(Tag.class, tagId);
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }

        return tag;
    }

    @Override
    public Long create(String tagName) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        Tag tag = new Tag(null, tagName, null);
        try {
            transaction = session.beginTransaction();
            session.save(tag);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return tag.getTagId();
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Tag tag = (Tag) session.load(Tag.class, tagId);
            session.delete(tag);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void edit(Tag tag) throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(tag);
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public List<Tag> getAllTags() throws DAOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        List<Tag> result = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Tag.class);
            criteria.addOrder(Order.asc("tagName"));
            result = criteria.list();
            transaction.commit();
        } catch (Exception e) {
            throw new DAOException(e);
        } finally {
            closeSession(session);
        }
        return result;
    }

    private void closeSession(Session session) {
        if (session.isOpen()) {
            session.close();
        }
    }

}
