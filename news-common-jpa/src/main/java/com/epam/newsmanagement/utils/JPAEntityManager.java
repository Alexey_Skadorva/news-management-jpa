package com.epam.newsmanagement.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAEntityManager {
    public static final String NEWS_JPA_UNIT = "newsJPAUnit";
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(NEWS_JPA_UNIT);

    public JPAEntityManager(String jpaUnit) {
        factory = Persistence.createEntityManagerFactory(jpaUnit);
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        return factory;
    }
}
