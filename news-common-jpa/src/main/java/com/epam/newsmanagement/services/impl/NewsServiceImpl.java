package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.AuthorService;
import com.epam.newsmanagement.services.NewsService;
import com.epam.newsmanagement.services.TagService;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class NewsServiceImpl implements NewsService {
    private static Logger logger = Logger.getLogger(NewsServiceImpl.class);
    private NewsDao newsDao;
    private TagService tagService;
    private AuthorService authorService;


    @Override
    public Long addNews(News news)
            throws ServiceException {
        Long insertId = null;
        try {
            insertId = newsDao.create(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return insertId;
    }

    @Override
    public void editNews(News news) throws ServiceException {
        try {
            newsDao.edit(news);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long saveNews(News news, Date date, List<Long> tagIds, Long authorId) throws ServiceException {
        Date modificationDate = new Date(date.getTime());
        news.setModificationDate(modificationDate);
        Author author = authorService.getAuthor(authorId);
        List<Tag> tags = new ArrayList<>();
        if (tagIds != null) {
            for (Long i : tagIds) {
                tags.add(tagService.getTag(i));
                System.out.print(tagService.getTag(i) + " ");
            }
        }
        news.setTags(tags);
        news.setAuthor(author);
        Long newsId = news.getNewsId();
        try {
            if (news.getNewsId() == null) {
                Timestamp creationDate = new Timestamp(date.getTime());
                news.setCreationDate(creationDate);
                newsId = newsDao.create(news);
            } else {
                News oldNews = getNews(newsId);
                news.setComments(oldNews.getComments());
                news.setCreationDate(oldNews.getCreationDate());
                newsDao.edit(news);
            }
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return newsId;
    }

    @Override
    public void deleteNews(List<Long> newsIds) throws ServiceException {
        try {
            for (Long id : newsIds) {
                newsDao.delete(id);
            }
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News getNews(Long newsId) throws ServiceException {
        News news = null;
        try {
            news = newsDao.get(newsId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return news;
    }

    public List<News> findByFilters(SearchCriteria searchCriteria, int pageId, int NUM_NEWS_ON_PAGE) throws ServiceException {
        List<News> news = null;
        try {
            news = newsDao.findNewsByFilters(searchCriteria, pageId, NUM_NEWS_ON_PAGE);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return news;
    }

    public int countNumNews(SearchCriteria searchCriteria) throws ServiceException {
        int number = 0;
        try {
            number = newsDao.countNumNews(searchCriteria);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return number;
    }

    public void setNewsDao(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }
}
