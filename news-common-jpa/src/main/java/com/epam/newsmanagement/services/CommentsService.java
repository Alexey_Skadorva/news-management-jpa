package com.epam.newsmanagement.services;

import com.epam.newsmanagement.exception.ServiceException;

public interface CommentsService {
    /**
     * Adds comment to the news.
     * Adds a comment in the table Comments.
     *
     * @return
     * @throws ServiceException
     */
    Long addComment(String commentText, Long newsId) throws ServiceException;

    /**
     * Removes the comment from the table
     * Comments, by a unique id
     *
     * @throws ServiceException
     */
    void deleteComment(Long commentId) throws ServiceException;
}
