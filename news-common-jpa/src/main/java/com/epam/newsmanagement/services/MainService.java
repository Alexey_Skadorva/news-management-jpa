package com.epam.newsmanagement.services;

import com.epam.newsmanagement.exception.ServiceException;

import javax.servlet.http.HttpSession;

public interface MainService {

    /**
     * Finds previous news id
     *
     * @param newsId,session,numNewsOnPage
     * @return id
     * @throws ServiceException
     */
    Long findPreviousNewsId(Long newsId, HttpSession session, int numNewsOnPage) throws ServiceException;

    /**
     * Finds next news id
     *
     * @param newsId,session,numNewsOnPage
     * @return id
     * @throws ServiceException
     */
    Long findNextNewsId(Long newsId, HttpSession session, int numOfPages, int numNewsOnPage) throws ServiceException;

}
