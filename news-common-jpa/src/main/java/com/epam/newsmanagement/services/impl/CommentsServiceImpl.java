package com.epam.newsmanagement.services.impl;

import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.CommentsService;
import com.epam.newsmanagement.services.NewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;

public class CommentsServiceImpl implements CommentsService {
    private static Logger logger = Logger.getLogger(CommentsServiceImpl.class);

    private CommentsDao commentsDao;

    @Autowired
    private NewsService newsService;

    @Override
    public Long addComment(String commentText, Long newsId) throws ServiceException {
        Long commentId = null;
        try {
            Timestamp creationDate = new Timestamp(new java.util.Date().getTime());
            News news = newsService.getNews(newsId);
            Comment comment = new Comment(null, commentText, creationDate, news);
            commentId = commentsDao.create(comment);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
        return commentId;
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentsDao.delete(commentId);
        } catch (DAOException e) {
            logger.error(e);
            throw new ServiceException(e);
        }
    }

    public void setCommentsDao(CommentsDao commentsDao) {
        this.commentsDao = commentsDao;
    }
}
