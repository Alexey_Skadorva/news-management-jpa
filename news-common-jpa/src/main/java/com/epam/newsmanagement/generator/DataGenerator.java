package com.epam.newsmanagement.generator;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.CommentsDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.hibernate.AuthorDaoImpl;
import com.epam.newsmanagement.dao.hibernate.CommentsDaoImpl;
import com.epam.newsmanagement.dao.hibernate.NewsDaoImpl;
import com.epam.newsmanagement.dao.hibernate.TagDaoImpl;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataGenerator {
    private final int numberOfRecords = 10000;
    AuthorDao authorDao = new AuthorDaoImpl();
    TagDao tagDao = new TagDaoImpl();
    NewsDao newsDao = new NewsDaoImpl();
    CommentsDao commentsDao = new CommentsDaoImpl();
    Random random = new Random();

    public void main() throws DAOException {
        List<Long> tagIds = new ArrayList<>();
        List<Long> authorIds = new ArrayList<>();
        List<Long> newsIds = new ArrayList<>();

//        for (int i = 0; i < 10000; i++) {
//            String authorName = generateData();
//            Long insetId = authorDao.create(authorName);
//            authorIds.add(insetId);
//        }
//
//        for (int i = 0; i < 10000; i++) {
//            String tagName = generateData();
//            Long insetId = tagDao.create(tagName);
//            tagIds.add(insetId);
//        }

//        for (int i = 0; i < 10000; i++) {
//            String title = generateData();
//            String shortText = generateData();
//            String fullText = generateData();
//            Timestamp creationDate =  new Timestamp(1427369733);
//            Date modificationDate = new Date(1427369733);
//            Author author = authorDao.get(1L);
//            List<Tag> tags = new ArrayList<>();
//            tags.add(tagDao.get(1L));
//
//            News news = new News(null, shortText, fullText, title, creationDate, modificationDate, author, tags, null);
//            Long insertId = newsDao.create(news);
//            newsIds.add(insertId);
//        }

        for (int i = 0; i < 10000; i++) {
            String commentText = generateData();
            Timestamp creationDate = new Timestamp(1427369733);
            News news = newsDao.get(1L);
            Comment comment = new Comment(null, commentText, creationDate, news);
            commentsDao.create(comment);
        }
    }

    public String generateData() {
        char[] chars = "qwertyuiopasdfghjklzxcvbnm".toCharArray();
        StringBuilder sb = new StringBuilder();
        int number = random.nextInt(20) + 1;
        for (int i = 0; i < number; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }

    public void setTagDao(TagDao tagDao) {
        this.tagDao = tagDao;
    }

    public void setNewsDao(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    public void setCommentsDao(CommentsDao commentsDao) {
        this.commentsDao = commentsDao;
    }

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

}
