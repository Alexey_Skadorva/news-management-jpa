package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "TAG")
public class Tag implements Serializable {
    private static final long serialVersionUID = -1340917654956509396L;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "NEWS_TAG",
            joinColumns = {@JoinColumn(name = "TAG_ID")},
            inverseJoinColumns = {@JoinColumn(name = "NEWS_ID")}
    )
    List<News> news;
    @Id
    @GeneratedValue(generator = "tag_seq")
    @SequenceGenerator(name = "tag_seq", sequenceName = "TAG_SEQ", allocationSize = 1)
    @Column(name = "TAG_ID", nullable = false, unique = true)
    private Long tagId;
    @Column(name = "TAG_NAME")
    private String tagName;

    public Tag(Long tagId, String tagName, List<News> news) {
        this.tagId = tagId;
        this.tagName = tagName;
        this.news = news;
    }

    public Tag() {

    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    @Override
    public String toString() {
        return tagId + " " + tagName;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + tagId);
        result = result + tagName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tag tag = (Tag) obj;
        if (tagId != tag.tagId)
            return false;
        if (!tagName.equals(tag.tagName))
            return false;
        return true;
    }
}
