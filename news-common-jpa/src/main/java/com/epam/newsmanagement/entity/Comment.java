package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {
    private static final long serialVersionUID = -7647808599920180493L;

    @Id
    @GeneratedValue(generator = "comments_seq")
    @SequenceGenerator(name = "comments_seq", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
    @Column(name = "COMMENT_ID", nullable = false, unique = true)
    private Long commentId;

    @Column(name = "COMMENT_TEXT")
    private String commentText;

    @Column(name = "CREATION_DATE")
    private Timestamp creationDate;

    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinColumn(name = "NEWS_ID")
    private News news;

    public Comment(Long commentId, String commentText,
                   Timestamp creationDate, News news) {
        this.commentId = commentId;
        this.commentText = commentText;
        this.creationDate = creationDate;
        this.news = news;
    }

    public Comment() {
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }


    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }


    @Override
    public String toString() {
        return commentId + " " + commentText + " "
                + creationDate;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + commentId);
        result = result + commentText.hashCode();
        result = result + creationDate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Comment comment = (Comment) obj;
        if (commentId != comment.commentId)
            return false;
        if (!commentText.equals(comment.commentText))
            return false;
        if (!comment.creationDate.equals(creationDate))
            return false;
        return true;
    }
}
