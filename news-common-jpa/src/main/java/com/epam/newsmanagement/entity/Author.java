package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {
    private static final long serialVersionUID = -933563462559904791L;

    @Id
    @GeneratedValue(generator = "author_seq")
    @SequenceGenerator(name = "author_seq", sequenceName = "AUTHOR_SEQ", allocationSize = 1)
    @Column(name = "AUTHOR_ID", nullable = false, unique = true)
    private Long authorId;

    @Column(name = "AUTHOR_NAME")
    private String authorName;

    @Column(name = "EXPIRED")
    private Timestamp expired;

    public Author(Long authorId, String authorName, Timestamp expired) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.expired = expired;
    }

    public Author() {

    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Override
    public String toString() {
        return authorId + " " + authorName;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + authorId);
        result = result + authorName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Author author = (Author) obj;
        if (authorId != author.authorId)
            return false;
        if (!authorName.equals(author.authorName))
            return false;
        return true;
    }
}
