package com.epam.newsmanagement.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "NEWS")
public class News implements Serializable {

    private static final long serialVersionUID = -6891276437253024367L;

    @Id
    @GeneratedValue(generator = "news_seq")
    @SequenceGenerator(name = "news_seq", sequenceName = "NEWS_SEQ", allocationSize = 1)
    @Column(name = "NEWS_ID", nullable = false, unique = true)
    private Long newsId;

    @Column(name = "SHORT_TEXT")
    private String shortText;

    @Column(name = "FULL_TEXT")
    private String fullText;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CREATION_DATE")
    private Timestamp creationDate;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(
            name = "NEWS_AUTHOR",
            joinColumns = {@JoinColumn(name = "NEWS_ID")},
            inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID")}
    )
    private Author author;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "NEWS_TAG",
            joinColumns = {@JoinColumn(name = "NEWS_ID")},
            inverseJoinColumns = {@JoinColumn(name = "TAG_ID")}
    )
    private List<Tag> tags;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "NEWS_ID")
    @OrderBy("creationDate DESC")
    private List<Comment> comments;

//    @Version
//   // @Column(name="OPTLOCK")
//    Integer version;

    public News(Long newsId, String shortText, String fullText, String title, Timestamp creationDate, Date modificationDate
            , Author author, List<Tag> tags, List<Comment> comments) {
        this.newsId = newsId;
        this.shortText = shortText;
        this.fullText = fullText;
        this.title = title;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
        this.author = author;
        this.tags = tags;
        this.comments = comments;
    }

    public News() {
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tag) {
        this.tags = tag;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

//    public Integer getVersion() {
//        return version;
//    }
//
//    public void setVersion(Integer version) {
//        this.version = version;
//    }

    @Override
    public String toString() {
        return newsId + " " + shortText + " " + fullText + " " + title + " "
                + creationDate + " " + modificationDate;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = (int) (result + newsId);
        result = result + shortText.hashCode();
        result = result + fullText.hashCode();
        result = result + title.hashCode();
        result = result + creationDate.hashCode();
        result = result + modificationDate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        News news = (News) obj;
        if (fullText.equals(news.fullText))
            return false;
        if (shortText.equals(news.shortText))
            return false;
        if (!title.equals(news.title))
            return false;
        if (!creationDate.equals(news.creationDate))
            return false;
        if (!modificationDate.equals(news.modificationDate))
            return false;
        return true;
    }
}
