package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alexey_Skadorva on 7/9/2015.
 */
public class SearchCriteria implements Serializable {
    private static final long serialVersionUID = 1462432370486015638L;

    private Long authorId;
    private List<Long> tagsId;

    public SearchCriteria(Long authorId, List<Long> tagsId) {
        this.authorId = authorId;
        this.tagsId = tagsId;
    }

    public SearchCriteria() {

    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

}
