<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
  <head>
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet" />
    <script src="<c:url value="/resources/js/script.js" />"></script>
    <title> <tiles:getAsString name="title" /></title>
  </head>
  <body>
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="menu"/>
    <tiles:insertAttribute name="content"/>
    <tiles:insertAttribute name="footer"/>
  </body>
</html>
