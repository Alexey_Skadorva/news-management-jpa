<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<style>
	<%@include file='/resources/css/style.css' %>
</style>

<div class="header"><fmt:message key="news.header" />
	<div class="loginInfo">
		<sec:authorize access="isAuthenticated()">
			<div class="name"><fmt:message key="login.message.hello"/><sec:authentication property="principal.username" /></div>
			<form action="<spring:url value="/logout"/>" class="logout-button">
				<input class="submitLogout" type="submit" value="<fmt:message key="login.button.logout"/>" />
			</form>
		</sec:authorize>
	</div>

	<div class="locale">
		<a href="?language=en_EN">EN</a>|<a href="?language=ru_RU">RU</a>
	</div>
</div>



