<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="left-box">
  <div class="left-up-box">
    <div class="admin-menu">
      <ul>
        <li>
          <a href="<spring:url value="/reset_filter"/>" ><fmt:message key="menu.news-list" /> </a>
        </li>
        <li>
          <a href="<spring:url value="/add_news_link"/>"><fmt:message key="menu.add.news"/> </a>
        </li>
        <li>
          <a href="<spring:url value="/change_authors"/>"><fmt:message key="menu.add.author" /></a>
        </li>
        <li>
          <a href="<spring:url value="/change_tags"/>"><fmt:message key="menu.add.tags" /> </a>
        </li>
      </ul>
    </div>
  </div>
</div>
