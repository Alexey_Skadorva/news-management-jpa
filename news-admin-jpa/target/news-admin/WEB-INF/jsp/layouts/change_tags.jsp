<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="box">
    <div class="changeTagsAndAuthors">
      <c:if test="${tags != null}">
        <c:forEach var="i" begin="0" end="${fn:length(tags)-1}">
          <article>
              <form method="POST" id="updateForm${tags.get(i).getTagId()}" action="<spring:url value="/update_tag"/>" >
                  <b>   <fmt:message key="tag.tag"/>:</b>
                  <input class="updateForm"  type="text" id="text${tags.get(i).getTagId()}" name="tagName" value="${tags.get(i).getTagName()}" disabled required/>
                  <input type="hidden"   name="tagId" value="${tags.get(i).getTagId()}"/>
                  <a id="edit${tags.get(i).getTagId()}" href="javascript:{}" onclick="switchEnableTagInput(${tags.get(i).getTagId()})">
                      <fmt:message key="author.edit"/></a>
                  <a id="update${tags.get(i).getTagId()}" href="javascript:{}" onclick="updateFormLinkTag(${tags.get(i).getTagId()})" class="hidden-link">
                      <fmt:message key="author.update"/> </a>
                  <a id="delete${tags.get(i).getTagId()}" href="<spring:url value="/delete_tag/${tags.get(i).getTagId()}"/>"
                  class="hidden-link" >
                      <fmt:message key="author.delete"/></a>
                  <a class="hidden-link" id="cancel${tags.get(i).getTagId()}" href="javascript:{}" onclick="cancelTagInput(${tags.get(i).getTagId()})">
                      <fmt:message key="author.cancel"/></a><br />
              </form>
          </article>
      </c:forEach>
      </c:if>
        <form  method="POST" id="save" name="add_tag" action="<spring:url value="/add_tag"/>" class="addTagAndAuthor">
          <b> <fmt:message key="tag.add"/>:</b>
            <input   class="saveForm" type="text" name="tagName" required/>
            <a href="javascript:{}" onclick="saveFormTag()"><fmt:message key="author.save"/></a>
        </form>
                   <div id="validateEmpty" class="hidden-validate" ><fmt:message key="news.validateEmpty"/></div>
           <div id="validate" class="hidden-validate" ><fmt:message key="news.validateSize"/></div>
    </div>
</div>
