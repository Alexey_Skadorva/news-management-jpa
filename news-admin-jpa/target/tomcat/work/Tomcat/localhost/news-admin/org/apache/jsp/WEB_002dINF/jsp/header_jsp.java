/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.47
 * Generated at: 2015-07-22 08:12:36 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.WEB_002dINF.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class header_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.HashMap<java.lang.String,java.lang.Long>(1);
    _jspx_dependants.put("/resources/css/style.css", Long.valueOf(1437545490399L));
  }

  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005faccess;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fsec_005fauthentication_0026_005fproperty_005fnobody;
  private org.apache.jasper.runtime.TagHandlerPool _005fjspx_005ftagPool_005fspring_005furl_0026_005fvalue_005fnobody;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005faccess = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fsec_005fauthentication_0026_005fproperty_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _005fjspx_005ftagPool_005fspring_005furl_0026_005fvalue_005fnobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
    _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.release();
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005faccess.release();
    _005fjspx_005ftagPool_005fsec_005fauthentication_0026_005fproperty_005fnobody.release();
    _005fjspx_005ftagPool_005fspring_005furl_0026_005fvalue_005fnobody.release();
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<style>\r\n");
      out.write("\t");
      out.write("body {\r\n");
      out.write("    font-family: 'Open Sans', sans-serif;\r\n");
      out.write("    margin:0 auto;\r\n");
      out.write("    width:1000px;\r\n");
      out.write("    position: relative;\r\n");
      out.write("\tborder: 3px solid ;\r\n");
      out.write("}\r\n");
      out.write(".title span{\r\n");
      out.write("\t\tpadding-left:30px;\r\n");
      out.write("\tfloar:left;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write(".title{\r\n");
      out.write("\tfloar:left;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write(".header{\r\n");
      out.write("\tpadding-left:30px;\r\n");
      out.write("\tpadding-top:20px;\r\n");
      out.write("\tfont: bold 34px Arial,Helvetica,Sans-serif;\r\n");
      out.write("\theight:60px;\r\n");
      out.write("\tborder-style: solid;\r\n");
      out.write("\tborder-width: 0px 0px 3px 0px;\r\n");
      out.write("}\r\n");
      out.write(".reset{\r\n");
      out.write("\tmargin-left:100px;\r\n");
      out.write("}\r\n");
      out.write(".footer{\r\n");
      out.write("\tpadding-left:350px;\r\n");
      out.write("\tpadding-top:5px;\r\n");
      out.write("\tborder-style: solid;\r\n");
      out.write("\tborder-width: 3px 0px 0px 0px;\r\n");
      out.write("}\r\n");
      out.write(".date{\r\n");
      out.write("\tpadding-left:800px;display:inline-block;\r\n");
      out.write("}\r\n");
      out.write(".dateForAdmin{\r\n");
      out.write("\tmargin-left:650px;\r\n");
      out.write("\tpadding-top:-30px;\r\n");
      out.write("\tfloat:right;\r\n");
      out.write("\tposition: relative;\r\n");
      out.write("\ttext-decoration: underline;\r\n");
      out.write("\ttop:-20px;\r\n");
      out.write("}\r\n");
      out.write(".infoAdoutNews{\r\n");
      out.write("\tposition: relative;\r\n");
      out.write("\tpadding-top:10px;\r\n");
      out.write("\tpadding-left:550px;\r\n");
      out.write("\theight:100px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".navigation{padding-left:170px;}\r\n");
      out.write(".navigation select{width:150px;}\r\n");
      out.write(".box{position:relative;\r\n");
      out.write("\tborder-style: solid;\r\n");
      out.write("\tborder-width: 1px;\r\n");
      out.write("\tborder-color: silver;\r\n");
      out.write("\tpadding-top:10px ;\r\n");
      out.write("\tpadding-left:10px ;\r\n");
      out.write("\tpadding-right:10px ;\r\n");
      out.write("\tmargin-top:5px ;\r\n");
      out.write("\tmargin-left:200px ;\r\n");
      out.write("\tmargin-right:5px ;\r\n");
      out.write("\tmargin-bottom:5px ;\r\n");
      out.write("\theight: 85%;\r\n");
      out.write("}\r\n");
      out.write(".boxLogin{\r\n");
      out.write("\tposition:relative;\r\n");
      out.write("\tborder-style: solid;\r\n");
      out.write("\tborder-width: 1px;\r\n");
      out.write("\tborder-color: silver;\r\n");
      out.write("\tpadding-top:10px ;\r\n");
      out.write("\tpadding-left:10px ;\r\n");
      out.write("\tpadding-right:10px ;\r\n");
      out.write("\tmargin-top:5px ;\r\n");
      out.write("\tmargin-left:10px ;\r\n");
      out.write("\tmargin-right:5px ;\r\n");
      out.write("\tmargin-bottom:5px ;\r\n");
      out.write("\tmin-height: 100%;\r\n");
      out.write("}\r\n");
      out.write(".tags{\r\n");
      out.write("\tcolor: silver;\r\n");
      out.write("\tdisplay:inline-block;\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tright:250px;\r\n");
      out.write("\ttop:65px;\r\n");
      out.write("}\r\n");
      out.write(".comments{\r\n");
      out.write("\tcolor:red;\r\n");
      out.write("\ttext-align: left;\r\n");
      out.write("\tdisplay:inline-block;\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\ttop:32px;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write(".pageNavigator{\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\tbottom:10px;\r\n");
      out.write("\tbackground:silver;\r\n");
      out.write("\theight:100px;\r\n");
      out.write("\twidth:967px;\r\n");
      out.write("}\r\n");
      out.write(".newsMessage{\r\n");
      out.write("\tpadding-top:5px ;\r\n");
      out.write("\tpadding-left:40px ;\r\n");
      out.write("}\r\n");
      out.write(".commentText{\r\n");
      out.write("\tbackground:silver;\r\n");
      out.write("\twidth:300px;\r\n");
      out.write("}\r\n");
      out.write(".addComment{\r\n");
      out.write("\tmargin-top:35px;\r\n");
      out.write("\twidth:300px;\r\n");
      out.write("\theight:100px;\r\n");
      out.write("}\r\n");
      out.write("#commentText{\r\n");
      out.write("\twidth:300px;\r\n");
      out.write("\theight:100px;\r\n");
      out.write("}\r\n");
      out.write("#buttonAddComment{\r\n");
      out.write("\tmargin-top:5px;\r\n");
      out.write("\tmargin-left:170px;\r\n");
      out.write("\theight: 35px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".change-tags{margin-left:200px;}\r\n");
      out.write(".admin-menu{float: left;background:white;margin-left:15px;margin-right:15px;}\r\n");
      out.write(".admin-menu a{margin-top: 5px;font-size: 15px;}\r\n");
      out.write(".left-box{\r\n");
      out.write("\twidth:200px;\r\n");
      out.write("\theight:1000px;\r\n");
      out.write("\tfloat: left;\r\n");
      out.write("\tmargin-top: 5px;\r\n");
      out.write("\tmargin-right: 5px;\r\n");
      out.write("}\r\n");
      out.write(".left-up-box{\r\n");
      out.write("\theight:150px;\r\n");
      out.write("\tbackground:gainsboro;\r\n");
      out.write("\tfloat: left;\r\n");
      out.write("}\r\n");
      out.write(".locale{padding-left:915px;\r\n");
      out.write("font-size: 16px;}\r\n");
      out.write(".box{font-family: sans-serif,Arial}\r\n");
      out.write(".paging{\r\n");
      out.write("\tbackground:ghostwhite;\r\n");
      out.write("\tpadding-top:30px;\r\n");
      out.write("\twidth:784px;\r\n");
      out.write("\theight: 50px;\r\n");
      out.write("\tmargin-left: -5px;\r\n");
      out.write("\ttext-align: center;\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\ttop:89%;\r\n");
      out.write("}\r\n");
      out.write(".paging a{\r\n");
      out.write("\r\n");
      out.write("\tbackground:whitesmoke;\r\n");
      out.write("\tborder:solid black 2px;\r\n");
      out.write("\tcolor:black;\r\n");
      out.write("\tfont:normal 11px/11px Verdana, Helvetica, sans-serif;\r\n");
      out.write("\tpadding:7px;\r\n");
      out.write("\tmargin:3px 0px 0px 0px;\r\n");
      out.write("\ttext-align:center;\r\n");
      out.write("\ttext-decoration:none;\r\n");
      out.write("}\r\n");
      out.write(".delete{\r\n");
      out.write("\tmargin-left:700px;\r\n");
      out.write("\twidth: 80px;\r\n");
      out.write("\theight: 40px;\r\n");
      out.write("}\r\n");
      out.write(".shortNews{padding-bottom: -20px;}\r\n");
      out.write(".editNews{\r\n");
      out.write("\tmargin-left:140px;\r\n");
      out.write("\tmargin-top:80px;\r\n");
      out.write("}\r\n");
      out.write(".editNews article{padding-top:10px;}\r\n");
      out.write(".changeTagsAndAuthors{\r\n");
      out.write("\tpadding-left:50px;\r\n");
      out.write("\tpadding-top:60px;\r\n");
      out.write("}\r\n");
      out.write(".changeTagsAndAuthors text{\r\n");
      out.write("\tfloat: left;\r\n");
      out.write("\tpadding-right: 70px;\r\n");
      out.write("}\r\n");
      out.write(".changeTagsAndAuthors input [type=\"text\"]{width: 600px;}\r\n");
      out.write(".addTagAndAuthor{padding-top: 30px;}\r\n");
      out.write("#addNews article {\r\n");
      out.write("\tposition:relative;\r\n");
      out.write("\tpadding-top: 15px;\r\n");
      out.write("\tpadding-bottom: 10px;\r\n");
      out.write("\tvertical-align:middle;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("#addNews {\r\n");
      out.write("\tpadding-left: 20px;\r\n");
      out.write("\tpadding-top: 50px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".infoNews{\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tmargin-left:80px;\r\n");
      out.write("\twidth:500px;\r\n");
      out.write("\tleft:50px;\r\n");
      out.write("}\r\n");
      out.write(".changeDate{\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tmargin-left:80px;\r\n");
      out.write("\twidth:200px;\r\n");
      out.write("\tleft:50px;\r\n");
      out.write("}\r\n");
      out.write(".area{\r\n");
      out.write("margin-top:55px;\r\n");
      out.write("}\r\n");
      out.write(".changeTitle{\r\n");
      out.write("\tmargin-top:10px;\r\n");
      out.write("\twidth:400px;\r\n");
      out.write("\theight:30px;\r\n");
      out.write("}\r\n");
      out.write(".addTitle{\r\n");
      out.write("\tmargin-top:10px;\r\n");
      out.write("\twidth:300px;\r\n");
      out.write("\tmargin-left:50px;\r\n");
      out.write("}\r\n");
      out.write(".hidden-link{\r\n");
      out.write("\tvisibility: hidden;\r\n");
      out.write("\tpadding-left:5px;\r\n");
      out.write("}\r\n");
      out.write(".hidden-validate{\r\n");
      out.write("\tvisibility: hidden;\r\n");
      out.write("\twidth:500px;\r\n");
      out.write("\tmargin-top:40px;\r\n");
      out.write("}\r\n");
      out.write(".hidden-validate-comment{\r\n");
      out.write("\tvisibility: hidden;\r\n");
      out.write("\tpadding-top:20px;\r\n");
      out.write("}\r\n");
      out.write(".login{\r\n");
      out.write("\tpadding-left:400px;\r\n");
      out.write("\tpadding-top:150px;\r\n");
      out.write("\tmargin-bottom:230px;\r\n");
      out.write("}\r\n");
      out.write(".addFullText{\r\n");
      out.write("\tmargin-top:10px;\r\n");
      out.write("\tmargin-left:50px;\r\n");
      out.write("}\r\n");
      out.write(".changeNews{\r\n");
      out.write("\tpadding-left:-200px;\r\n");
      out.write("}\r\n");
      out.write(".deleteButtom{\r\n");
      out.write("\tmargin-left:275px;\r\n");
      out.write("\twidth: 24px;\r\n");
      out.write("\theight: 20px;\r\n");
      out.write("\tfloat:left;\r\n");
      out.write("}\r\n");
      out.write(".commentsNews{margin-top:50px;}\r\n");
      out.write(".ui-widget{\r\n");
      out.write("\tbackground-color: white;\r\n");
      out.write("\tborder: 1px solid;\r\n");
      out.write("\tborder-color: #adadad;\r\n");
      out.write("\tmargin-right: 5px;\r\n");
      out.write("\tposition: relative;\r\n");
      out.write("\ttop: 5px;\r\n");
      out.write("}\r\n");
      out.write(".filter{\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tmargin-right:55px;\r\n");
      out.write("\tfloat: left;\r\n");
      out.write("\tmargin-top:-37px;\r\n");
      out.write("\tmargin-left:325px;\r\n");
      out.write("}\r\n");
      out.write(".second{\r\n");
      out.write("\tmargin-left:290px;\r\n");
      out.write("}\r\n");
      out.write(".shortText{\r\n");
      out.write("\twidth:750px;\r\n");
      out.write("\tfloat:left;\r\n");
      out.write("\tz-index:1;\r\n");
      out.write("}\r\n");
      out.write(".fullText{\r\n");
      out.write("\twidth:600px;\r\n");
      out.write("\tpadding-bottom: 20px;\r\n");
      out.write("}\r\n");
      out.write(".changeFullText{\r\n");
      out.write("\twidth:600px;\r\n");
      out.write("\tpadding-bottom: 20px;\r\n");
      out.write("}\r\n");
      out.write(".navigationEdit{\r\n");
      out.write("\tposition:relative;\r\n");
      out.write("\tpadding-left:-200px;}\r\n");
      out.write(".loginInfo{\r\n");
      out.write("\tpadding-left:800px;\r\n");
      out.write("\tfloat:left;\r\n");
      out.write("\tfont-size:16px;\r\n");
      out.write("\tmargin-top:-50px;\r\n");
      out.write("}\r\n");
      out.write(".loginInfo name{\r\n");
      out.write("\tpadding-left:-20px;\r\n");
      out.write("}\r\n");
      out.write(".submitLogout{\r\n");
      out.write("\tfloat:left;\r\n");
      out.write("\tmargin-top:-20px;\r\n");
      out.write("\tmargin-left:110px;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write(".previous{\r\n");
      out.write("\tposition: relative;\r\n");
      out.write("\ttop:85%;\r\n");
      out.write("\ttop:13px;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write(".next{\r\n");
      out.write("\tposition: relative;\r\n");
      out.write("\tleft:670px;\r\n");
      out.write("}\r\n");
      out.write(".multi-dropbox{\r\n");
      out.write("\tfloat:right;\r\n");
      out.write("}\r\n");
      out.write(".updateForm{\r\n");
      out.write("\tmargin-left:40px;\r\n");
      out.write("\twidth:300px;\r\n");
      out.write("\tmargin-bottom: 30px;\r\n");
      out.write("\tmargin-right: 20px;\r\n");
      out.write("}\r\n");
      out.write(".saveForm{\r\n");
      out.write("\tmargin-left:20px;\r\n");
      out.write("\twidth:300px;\r\n");
      out.write("}\r\n");
      out.write(".error{\r\n");
      out.write("\tmargin-top: 20px;\r\n");
      out.write("\tmargin-left:45px;\r\n");
      out.write("}\r\n");
      out.write(".errorComment{\r\n");
      out.write("\tmargin-top: 45px;\r\n");
      out.write("\tmargin-left:20px;\r\n");
      out.write("}\r\n");
      out.write(".changeAuthor{\r\n");
      out.write("\tpadding-top:160px;\r\n");
      out.write("\tpadding-left:150px;\r\n");
      out.write("}\r\n");
      out.write("#validateAuthor{\r\n");
      out.write("\ttop:450px;\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\tleft:150;\r\n");
      out.write("}\r\n");
      out.write(".changeFromList{\r\n");
      out.write("\twidth:140px;\r\n");
      out.write("}\r\n");
      out.write(".addNewsSave{\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\tmargin-top:200px;\r\n");
      out.write("\tpadding: 8px;\r\n");
      out.write("\tmargin-left: 550px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".loginForm{\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tleft:500px;\r\n");
      out.write("}\r\n");
      out.write(".lineLogin{margin-top:10px;}\r\n");
      out.write(".loginSubmit{\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tmargin-left:210px;\r\n");
      out.write("\ttop:295px;\r\n");
      out.write("\twidth:50px;\r\n");
      out.write("\tpadding:3px;}\r\n");
      out.write(".editlink{\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\ttop:65px;\r\n");
      out.write("\tright:0px;\r\n");
      out.write("\t}\r\n");
      out.write(".news{margin-top:35px;}\r\n");
      out.write(".commentDate{\r\n");
      out.write("\ttext-decoration: underline;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write("#validateTitleSize{\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\ttop:0px;\r\n");
      out.write("\tleft:130px;\r\n");
      out.write("}\r\n");
      out.write("#validateShortTextSize{\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\ttop:55px;\r\n");
      out.write("\tleft:130px;\r\n");
      out.write("}\r\n");
      out.write("#validateFullTextSize{\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\ttop:145px;\r\n");
      out.write("\tleft:130px;\r\n");
      out.write("}\r\n");
      out.write("#ulDropdown {\r\n");
      out.write("\tdisplay: inline-block;\r\n");
      out.write("\tborder:1px solid grey;\r\n");
      out.write("\twidth: 140px;\r\n");
      out.write("\tmargin-bottom: 10px;\r\n");
      out.write("\tposition: absolute;\r\n");
      out.write("\tleft: 340px;\r\n");
      out.write("\tbackground:white;\r\n");
      out.write("\tz-index:3;\r\n");
      out.write("\tfont-size:15px;\r\n");
      out.write("\tfont-color:grey;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("#ulDropdown li {\r\n");
      out.write("\tpadding-left:15px;\r\n");
      out.write("\tz-index:3;\r\n");
      out.write("\tbackground:white;\r\n");
      out.write("}\r\n");
      out.write(".createUlDropdown {\r\n");
      out.write("\r\n");
      out.write("\tdisplay:block;\r\n");
      out.write("\tbackground:blue;\r\n");
      out.write("\tmargin-left:-80px;\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write("#ulDrop:after {\r\n");
      out.write("\tcontent:\"\";\r\n");
      out.write("\tdisplay:inline-block;\r\n");
      out.write("\twidth:10px;\r\n");
      out.write("\theight:9px;\r\n");
      out.write("\tmargin-left:5px;\r\n");
      out.write("\tposition:absolute;\r\n");
      out.write("\tbackground: url(../images/triangle.jpg) no-repeat;\r\n");
      out.write("\ttop:5px;\r\n");
      out.write("\tright:5px;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".first-child-li-dropdown {\r\n");
      out.write("\tdisplay:block;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write(".dropdownTags {\r\n");
      out.write("\tdisplay:none;\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("</style>\r\n");
      out.write("\r\n");
      out.write("<div class=\"header\">");
      if (_jspx_meth_fmt_005fmessage_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t<div class=\"loginInfo\">\r\n");
      out.write("\t\t");
      if (_jspx_meth_sec_005fauthorize_005f0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("\r\n");
      out.write("\t<div class=\"locale\">\r\n");
      out.write("\t\t<a href=\"?language=en_EN\">EN</a>|<a href=\"?language=ru_RU\">RU</a>\r\n");
      out.write("\t</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_fmt_005fmessage_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.el.fmt.MessageTag _jspx_th_fmt_005fmessage_005f0 = (org.apache.taglibs.standard.tag.el.fmt.MessageTag) _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.get(org.apache.taglibs.standard.tag.el.fmt.MessageTag.class);
    _jspx_th_fmt_005fmessage_005f0.setPageContext(_jspx_page_context);
    _jspx_th_fmt_005fmessage_005f0.setParent(null);
    // /WEB-INF/jsp/header.jsp(10,20) name = key type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_fmt_005fmessage_005f0.setKey("news.header");
    int _jspx_eval_fmt_005fmessage_005f0 = _jspx_th_fmt_005fmessage_005f0.doStartTag();
    if (_jspx_th_fmt_005fmessage_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f0);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthorize_005f0(javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  sec:authorize
    org.springframework.security.taglibs.authz.JspAuthorizeTag _jspx_th_sec_005fauthorize_005f0 = (org.springframework.security.taglibs.authz.JspAuthorizeTag) _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005faccess.get(org.springframework.security.taglibs.authz.JspAuthorizeTag.class);
    _jspx_th_sec_005fauthorize_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthorize_005f0.setParent(null);
    // /WEB-INF/jsp/header.jsp(12,2) name = access type = null reqTime = true required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthorize_005f0.setAccess("isAuthenticated()");
    int _jspx_eval_sec_005fauthorize_005f0 = _jspx_th_sec_005fauthorize_005f0.doStartTag();
    if (_jspx_eval_sec_005fauthorize_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      out.write("\r\n");
      out.write("\t\t\t<div class=\"name\">");
      if (_jspx_meth_fmt_005fmessage_005f1(_jspx_th_sec_005fauthorize_005f0, _jspx_page_context))
        return true;
      if (_jspx_meth_sec_005fauthentication_005f0(_jspx_th_sec_005fauthorize_005f0, _jspx_page_context))
        return true;
      out.write("</div>\r\n");
      out.write("\t\t\t<form action=\"");
      if (_jspx_meth_spring_005furl_005f0(_jspx_th_sec_005fauthorize_005f0, _jspx_page_context))
        return true;
      out.write("\" class=\"logout-button\">\r\n");
      out.write("\t\t\t\t<input class=\"submitLogout\" type=\"submit\" value=\"");
      if (_jspx_meth_fmt_005fmessage_005f2(_jspx_th_sec_005fauthorize_005f0, _jspx_page_context))
        return true;
      out.write("\" />\r\n");
      out.write("\t\t\t</form>\r\n");
      out.write("\t\t");
    }
    if (_jspx_th_sec_005fauthorize_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005faccess.reuse(_jspx_th_sec_005fauthorize_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsec_005fauthorize_0026_005faccess.reuse(_jspx_th_sec_005fauthorize_005f0);
    return false;
  }

  private boolean _jspx_meth_fmt_005fmessage_005f1(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f0, javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.el.fmt.MessageTag _jspx_th_fmt_005fmessage_005f1 = (org.apache.taglibs.standard.tag.el.fmt.MessageTag) _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.get(org.apache.taglibs.standard.tag.el.fmt.MessageTag.class);
    _jspx_th_fmt_005fmessage_005f1.setPageContext(_jspx_page_context);
    _jspx_th_fmt_005fmessage_005f1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec_005fauthorize_005f0);
    // /WEB-INF/jsp/header.jsp(13,21) name = key type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_fmt_005fmessage_005f1.setKey("login.message.hello");
    int _jspx_eval_fmt_005fmessage_005f1 = _jspx_th_fmt_005fmessage_005f1.doStartTag();
    if (_jspx_th_fmt_005fmessage_005f1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f1);
      return true;
    }
    _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f1);
    return false;
  }

  private boolean _jspx_meth_sec_005fauthentication_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f0, javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  sec:authentication
    org.springframework.security.taglibs.authz.AuthenticationTag _jspx_th_sec_005fauthentication_005f0 = (org.springframework.security.taglibs.authz.AuthenticationTag) _005fjspx_005ftagPool_005fsec_005fauthentication_0026_005fproperty_005fnobody.get(org.springframework.security.taglibs.authz.AuthenticationTag.class);
    _jspx_th_sec_005fauthentication_005f0.setPageContext(_jspx_page_context);
    _jspx_th_sec_005fauthentication_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec_005fauthorize_005f0);
    // /WEB-INF/jsp/header.jsp(13,61) name = property type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_sec_005fauthentication_005f0.setProperty("principal.username");
    int _jspx_eval_sec_005fauthentication_005f0 = _jspx_th_sec_005fauthentication_005f0.doStartTag();
    if (_jspx_th_sec_005fauthentication_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005fsec_005fauthentication_0026_005fproperty_005fnobody.reuse(_jspx_th_sec_005fauthentication_005f0);
      return true;
    }
    _005fjspx_005ftagPool_005fsec_005fauthentication_0026_005fproperty_005fnobody.reuse(_jspx_th_sec_005fauthentication_005f0);
    return false;
  }

  private boolean _jspx_meth_spring_005furl_005f0(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f0, javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  spring:url
    org.springframework.web.servlet.tags.UrlTag _jspx_th_spring_005furl_005f0 = (org.springframework.web.servlet.tags.UrlTag) _005fjspx_005ftagPool_005fspring_005furl_0026_005fvalue_005fnobody.get(org.springframework.web.servlet.tags.UrlTag.class);
    _jspx_th_spring_005furl_005f0.setPageContext(_jspx_page_context);
    _jspx_th_spring_005furl_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec_005fauthorize_005f0);
    // /WEB-INF/jsp/header.jsp(14,17) name = value type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_spring_005furl_005f0.setValue("/logout");
    int[] _jspx_push_body_count_spring_005furl_005f0 = new int[] { 0 };
    try {
      int _jspx_eval_spring_005furl_005f0 = _jspx_th_spring_005furl_005f0.doStartTag();
      if (_jspx_th_spring_005furl_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (java.lang.Throwable _jspx_exception) {
      while (_jspx_push_body_count_spring_005furl_005f0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_spring_005furl_005f0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_spring_005furl_005f0.doFinally();
      _005fjspx_005ftagPool_005fspring_005furl_0026_005fvalue_005fnobody.reuse(_jspx_th_spring_005furl_005f0);
    }
    return false;
  }

  private boolean _jspx_meth_fmt_005fmessage_005f2(javax.servlet.jsp.tagext.JspTag _jspx_th_sec_005fauthorize_005f0, javax.servlet.jsp.PageContext _jspx_page_context)
          throws java.lang.Throwable {
    javax.servlet.jsp.PageContext pageContext = _jspx_page_context;
    javax.servlet.jsp.JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.el.fmt.MessageTag _jspx_th_fmt_005fmessage_005f2 = (org.apache.taglibs.standard.tag.el.fmt.MessageTag) _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.get(org.apache.taglibs.standard.tag.el.fmt.MessageTag.class);
    _jspx_th_fmt_005fmessage_005f2.setPageContext(_jspx_page_context);
    _jspx_th_fmt_005fmessage_005f2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_sec_005fauthorize_005f0);
    // /WEB-INF/jsp/header.jsp(15,53) name = key type = java.lang.String reqTime = false required = false fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
    _jspx_th_fmt_005fmessage_005f2.setKey("login.button.logout");
    int _jspx_eval_fmt_005fmessage_005f2 = _jspx_th_fmt_005fmessage_005f2.doStartTag();
    if (_jspx_th_fmt_005fmessage_005f2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f2);
      return true;
    }
    _005fjspx_005ftagPool_005ffmt_005fmessage_0026_005fkey_005fnobody.reuse(_jspx_th_fmt_005fmessage_005f2);
    return false;
  }
}
