package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    private static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class);
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = null;
        UserDetails userDetails = null;
        try {
            user = userService.findUser(login);
            if (user != null) {
                Set<GrantedAuthority> roles = new HashSet<GrantedAuthority>();
                roles.add(new SimpleGrantedAuthority(userService.findUserRole(user.getId())));
                userDetails =
                        new org.springframework.security.core.userdetails.User(user.getLogin(),
                                user.getPassword(),
                                roles);
            } else {
                userDetails = new org.springframework.security.core.userdetails.User("non", "non", new HashSet<GrantedAuthority>());
            }

        } catch (ServiceException e) {
            logger.error(e);
        }
        return userDetails;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
