package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * The controller is designed to interact with the authors.
 * The controller has methods add the author's change, layoffs.
 */
@Controller
public class AuthorsController {
    @Autowired
    AuthorService authorService;

    @RequestMapping(value = CHANGE_AUTHORS)
    public ModelAndView changeAuthors() throws ServiceException {
        ModelAndView mv = new ModelAndView(CHANGE_AUTHORS);
        List<Author> authors = authorService.getAllAuthors();
        mv.addObject("authors", authors);
        return mv;
    }

    @RequestMapping(value = UPDATE_AUTHOR)
    public ModelAndView updateAuthor(@RequestParam(value = "authorId", required = false) Long authorId,
                                     @RequestParam(value = "authorName", required = false) String authorName) throws ServiceException {
        Author author = new Author(authorId, authorName,null);
        authorService.editAuthor(author);
        return changeAuthors();
    }

    @RequestMapping(value = EXPIRE_AUTHOR)
    public ModelAndView deleteAuthor(@PathVariable Long authorId) throws ServiceException {
        authorService.expireAuthor(authorId);
        return changeAuthors();
    }

    @RequestMapping(value = ADD_AUTHOR)
    public ModelAndView addAuthor(ModelMap model, @RequestParam(value = "authorName", required = false) String authorName)
            throws ServiceException {
        authorService.addAuthor(authorName);
        return changeAuthors();
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler() {
        return new ModelAndView(ERROR);
    }
}
