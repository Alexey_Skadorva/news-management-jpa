package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.CommentsService;
import com.epam.newsmanagement.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * The controller is designed to interface with comments.
 * In the controller there are methods to add
 * and remove a comment.
 */
@Controller
public class CommentsController {
    @Autowired
    CommentsService commentsService;

    @Autowired
    NewsService newsService;

    @RequestMapping(value = ADD_COMMENT)
    public ModelAndView addComment(
            @RequestParam(value = "newsId", required = false) Long newsId,
            @RequestParam(value = "commentText", required = false) String commentText,HttpSession session) throws ServiceException {
        commentsService.addComment(commentText, newsId);
        News news = newsService.getNews(newsId);
        session.setAttribute("news", news);
        return new ModelAndView(NEWS_VIEW);
    }

    @RequestMapping(value = DELETE_COMMENT)
    public ModelAndView deleteComment(
            @RequestParam(value = "newsId", required = false) Long newsId,
            @RequestParam(value = "commentId", required = false) Long commentId,HttpSession session) throws ServiceException {
        commentsService.deleteComment(commentId);
        News news = newsService.getNews(newsId);
        session.setAttribute("news", news);
        return new ModelAndView(NEWS_VIEW);
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler() {
        return new ModelAndView(ERROR);
    }
}
