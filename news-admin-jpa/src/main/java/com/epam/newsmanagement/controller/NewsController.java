package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.services.AuthorService;
import com.epam.newsmanagement.services.MainService;
import com.epam.newsmanagement.services.NewsService;
import com.epam.newsmanagement.services.TagService;
import com.epam.newsmanagement.utils.SearchCriteriaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static com.epam.newsmanagement.utils.RequestMappingNames.*;

/**
 * The controller is designed to interface with news.
 * In the controller there are methods to add news,
 * edit, delete. There are methods responsible for news
 * search on parameters.
 */
@Controller
public class NewsController {
    @Autowired
    TagService tagService;
    @Autowired
    AuthorService authorService;
    @Autowired
    NewsService newsService;
    @Autowired
    MainService mainService;

    //@Value("${NUM}")
    private int NUM_NEWS_ON_PAGE = 4;

    private int numOfPages;

    @RequestMapping(value = PAGE)
    public ModelAndView listOfNews(@PathVariable(value = "pageId") Integer pageId,
                                   @RequestParam(value = "authorId", required = false) Long authorId,
                                   @RequestParam(value = "tagId", required = false) List<Long> tagsId,
                                   ModelMap model, HttpSession session) throws ServiceException {
        SearchCriteria searchCriteria = SearchCriteriaHelper.checkFilters(authorId, tagsId, pageId, session);
        if (pageId == null) {
            pageId = 1;
        }
        List<News> news =
                newsService.findByFilters(searchCriteria, pageId, NUM_NEWS_ON_PAGE);

        int numNews = newsService.countNumNews(searchCriteria);
        session.setAttribute("numNews", numNews);
        List<Long> newsIds = SearchCriteriaHelper.calculateNumberOfNews(news);
        session.setAttribute("newsIds", newsIds);
        numOfPages = SearchCriteriaHelper.calculateNumberOfPages(numNews, NUM_NEWS_ON_PAGE);
        model.addAttribute("numOfPages", numOfPages);
        getTagsAndAuthors(model);
        model.addAttribute("newsList", news);
        return new ModelAndView(MAIN);
    }

    @RequestMapping(value = NEWS)
    public ModelAndView getNews(@PathVariable Long newsId, HttpSession session) throws ServiceException {

        News news = newsService.getNews(newsId);
        SearchCriteriaHelper.changePage(session, newsId, NUM_NEWS_ON_PAGE);

        Long nextId = mainService.findNextNewsId(newsId, session, numOfPages, NUM_NEWS_ON_PAGE);
        Long previousId = mainService.findPreviousNewsId(newsId, session, NUM_NEWS_ON_PAGE);

        session.setAttribute("news", news);
        session.setAttribute("nextNewsId", nextId);
        session.setAttribute("previousNewsId", previousId);
        return new ModelAndView(NEWS_VIEW);
    }

    @RequestMapping(value = DELETE_NEWS)
    public ModelAndView deleteNews(@RequestParam(value = "newsIds", required = false) List<Long> newsIds,
                                   ModelMap model, HttpSession session) throws ServiceException {
        Integer pageId = (Integer) session.getAttribute("pageId");
        newsService.deleteNews(newsIds);
        return listOfNews(pageId, null, null, model, session);
    }

    @RequestMapping(value = ADD_NEWS_LINK)
    public ModelAndView addNewsLink(@PathVariable(value = "newsId") Long newsId, ModelMap model, HttpSession session)
            throws ServiceException {
        News news = null;
        if (newsId != null) {
            news = newsService.getNews(newsId);
        }
        getTagsAndAuthors(model);
        session.setAttribute("news", news);
        return new ModelAndView(ADD_NEWS_VIEW);
    }

    @RequestMapping(value = ADD_NEWS)
    public ModelAndView addNews(
            @RequestParam(value = "newsId", required = false) Long newsId,
            @RequestParam(value = "date", required = false) Date date,
            @RequestParam(value = "authorId", required = false) Long authorId,
            @RequestParam(value = "tagId", required = false) List<Long> tagsId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "shortText", required = false) String shortText,
            @RequestParam(value = "fullText", required = false) String fullText,
            ModelMap model, HttpSession session) throws ServiceException {
        News news = new News(newsId, shortText, fullText, title, null, null, null, null, null);
        newsId = newsService.saveNews(news, date,tagsId,authorId);
        return getNews(newsId, session);
    }

    public void getTagsAndAuthors(ModelMap model) throws ServiceException {
        List<Author> authors = authorService.getAllAuthors();
        List<Tag> tags = tagService.getAllTags();
        model.addAttribute("authors", authors);
        model.addAttribute("tags", tags);
    }

    @RequestMapping(value = RESET_FILTER)
    public ModelAndView resetFilter(ModelMap model, HttpSession session) throws ServiceException {
        session.setAttribute("searchCriteria", null);
        session.setAttribute("authorName", null);
        return listOfNews(null, null, null, model, session);
    }

    @ExceptionHandler(ServiceException.class)
    public ModelAndView serviceExceptionHandler() {
        return new ModelAndView(ERROR);
    }
}
