package com.epam.newsmanagement.utils;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey_Skadorva on 6/29/2015.
 */
public class SearchCriteriaHelper {

    public static void changePage(HttpSession session, Long newsId, int numberNewsOnPage) throws ServiceException {
        Integer pageId = (Integer) session.getAttribute("pageId");
        Integer previousNews = (Integer) session.getAttribute("nowNews");
        List<Long> newsIds = (List<Long>) session.getAttribute("newsIds");

        int currentIndex = newsIds.indexOf(newsId);
        if (previousNews != null) {
            if (currentIndex % numberNewsOnPage == numberNewsOnPage - 1 && previousNews % numberNewsOnPage == 0) {
                pageId--;
            }
            if (currentIndex % numberNewsOnPage == 0 && previousNews % numberNewsOnPage == numberNewsOnPage - 1) {
                pageId++;
            }
        }
        session.setAttribute("pageId", pageId);
        session.setAttribute("nowNews", currentIndex);
    }

    public static SearchCriteria checkFilters(Long authorId, List<Long> tagsId, Integer pageId, HttpSession session)
            throws ServiceException {
        SearchCriteria filter = (SearchCriteria) session.getAttribute("searchCriteria");
        if (filter != null) {
            if (tagsId == null) {
                tagsId = filter.getTagsId();
            }
            if (authorId == null) {
                authorId = filter.getAuthorId();
            } else {
                if (authorId == -1) {
                    authorId = null;
                }
            }
        }

        SearchCriteria searchCriteria = new SearchCriteria(authorId, tagsId);
        session.setAttribute("searchCriteria", searchCriteria);

        if (pageId == null) {
            pageId = 1;
        }
        session.setAttribute("pageId", pageId);
        session.setAttribute("nowNews", null);
        return searchCriteria;
    }

    public static List<Long> calculateNumberOfNews(List<News> news)
            throws ServiceException {
        List<Long> newsIds = new ArrayList<Long>();
        if(news!=null) {
            for (int i = 0; i < news.size(); i++) {
                newsIds.add(news.get(i).getNewsId());
            }
        }
        return newsIds;
    }

    public static int calculateNumberOfPages(int numNews, int numberNewsOnPage)
            throws ServiceException {
        int numOfPages = (numNews / numberNewsOnPage) + 1;
        return numOfPages;
    }
}
