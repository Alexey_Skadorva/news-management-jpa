<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="boxLogin">
  <form action="<spring:url value="/j_spring_security_check"/>" method="post" class="login">
    <h2><fmt:message key="login.enter" /></h2>
   <div class="lineLogin"> <fmt:message key="login.link.login" />:<input class="loginForm" type="text"  name="username"  required autofocus ><br/>
   </div>  <div class="lineLogin"><fmt:message key="login.password" />:
    <input  class="loginForm" type="password" name="password"  required ></div>
    <button class="loginSubmit" type="submit"><fmt:message key="login.enter" /></button>
  </form>
</div>