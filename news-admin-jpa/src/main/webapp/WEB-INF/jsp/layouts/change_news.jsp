<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="now" value="<%=new java.util.Date()%>" />
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript">
    $(document).ready(function() {
                $(".multi-dropbox").dropdownchecklist();
            }
    );
</script>

<div class="box">
    <form method="POST" action="<spring:url value="/add_news"/>" name="addNews" id="addNews"  onsubmit="return validateNews()" >
        <article>
             <b><fmt:message key="news.title"/>:</b>
            <input class="infoNews" type="text" name="title"  value="${news.getTitle()}" required />
                   <div id="validateTitleSize" class="hidden-validate" ><fmt:message key="news.validateTitleSize"/></div>
        </article>
        <article>
            <b><fmt:message key="news.date" />:</b>
            <c:choose>
            	<c:when test="${empty news.modificationDate }">
            		<input class="changeDate" type="date" name="date" value="<ctg:inputDateValue date='${now}'/>" />
                </c:when>
                <c:otherwise>
            		<input class="changeDate" type="date" name="date" value="<ctg:inputDateValue date='${news.modificationDate}'/>"/>
                 </c:otherwise>
             </c:choose>
        </article>
        <article>
            <b><fmt:message key="news.shortText" />:</b>
            <textarea class="infoNews" type="text" name="shortText" rows="5"
            cols="5" required><c:out value="${news.getShortText()}" /></textarea>
           <div id="validateShortTextSize" class="hidden-validate" ><fmt:message key="news.validateShortTextSize"/></div>
        </article>
       <article class="area">
           <b> <fmt:message key="news.fullText"/>:</b>
           <textarea  class="infoNews" type="text" name="fullText"
           rows="10" cols="50" required><c:out value="${news.getFullText()}"/></textarea>
         <div id="validateFullTextSize" class="hidden-validate" ><fmt:message key="news.validateFullTextSize"/></div>
       </article>
        <input type="hidden" name="newsId" value="${news.getNewsId()}"/>
        <input class="addNewsSave" type="submit" value="<fmt:message key="news.change.save"/>"/>

    <div class="changeAuthor" >
        <select name="authorId"  class="changeFromList" >
            <option value="-1"><fmt:message key="select.author"/></option>
            <c:forEach var="author" items="${authors}" varStatus="status">
                <option value="${ author.authorId }" >
                    <c:out value="${ author.authorName }" />
                </option>
            </c:forEach>
        </select>
    	<nav id="ulDropdown" class="ulDropdown">
                   <li id="ulDrop" class="first-child-li-dropdown">
                   <span id="ulDropdownText"><spring:message code="select.tags" /></span></li>
                   <c:forEach var="tag" items="${tags}" varStatus="status">
                    	<li class="dropdownTags"><input class="checkboxes-create-news" type="checkbox" value="${ tag.tagId }" name="tagId"/><span><c:out value="${ tag.tagName }" /></span></li>
                   </c:forEach>
         </nav>
        </div>
    </form>
       <div id="validateAuthor" class="hidden-validate" ><fmt:message key="news.validateAuthor"/></div>

</div>


