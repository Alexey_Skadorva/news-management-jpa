<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
  <head>
    <link href="<c:url value='/resources/css/style.css'/>" rel="stylesheet" />
    <script type="text/javascript" src="<c:url value='/resources/js/script.js'/>"></script>
    <title> <tiles:getAsString name="title" /></title>
  </head>
  <body>
    <tiles:insertAttribute name="header" />
    <tiles:insertAttribute name="menu"/>
    <tiles:insertAttribute name="content"/>
    <tiles:insertAttribute name="footer" />
  </body>
</html>
